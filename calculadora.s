    /* --------------------------------------------------------------------
    #   Autor : Miguel Angel Hernández Peñate
    #
    #   Nombre del archivo : calculadora.s
    #
    #   Objetivo: diseñar un programa que simule una calculadora de numeros entreros con signos.
    #   El programa debe mostrar un menu que le permita al usuario elegir la operacion a realizar:
    #                1-Suma, 2-Resta, 3-Multiplicacion, 4-Division, 5-Potenciacion
    ------------------------------------------------------------------------      */
/* --------------seccion para definir las variables------------------------------ */
.data
//-------------------------Mensaje de Bienvenida-----------------------------------
bienvenida: .asciz "\nBIENVENIDO AL PROGRAMA DE CALCULADORA \n Seleccione la operacion que desea realizar.\n"
menu: .asciz " 1-SUMA \n 2-RESTA \n 3-MULTIPLICACION \n 4-DIVISION \n 5-POTENCIACION\n"
//-------------------------Mensaje de ingreso de valores--------------------------
msjvalor1: .asciz "INGRESE EL PRIMER VALOR:  "
msjvalor2: .asciz "INGRESE EL SEGUNDO VALOR: "
//-------------------------Mensaje para volver------------------------------------
msjcontinuar: .asciz "Desea realizar otra operacion: \nSI - Preciona Cualquier Tecla \nNO - Preciona cero 0\n"
continuar: .word 0
//-------------------------Mensaje de Resultados----------------------------------
msjresultado: .asciz "\nEl resultado es: Total = %d \n"
//----------------------Mensaje de error de menu----------------------------------
error_menu: .asciz "\nLa opcion selecciona no existe!!!\n\nIngrese una operacion Correcta...\n"
//-----------------------Error de divisor------------------------------------------
errordiv: .asciz "No se puede dividir entre 0, Intenta otro digito\n"
//-----------------------Manipular archivos ---------------------------------------
errormsj:       .asciz "Error al crear el archivo"
nuevoarchivo:   .asciz "calculadorafile"
numero: .word 0
digito: .word 0
cifras: .word 0
salto: .word 10
descriptor: .space 4
dato: .word 0
signo: .asciz "-"
//-----------------------Variables------------------------------------------------
opcion: .word 0
valor1: .word 0
valor2: .word 0
total: .word 0
//------------------------Mensaje en el archivo-----------------------------------
msjsuma: .asciz "SUMA\n \nLOS DATOS FUERON GUARDADOS EN EL ARCHIVO calculadorafile\n"
msjresta: .asciz "RESTA\n \nLOS DATOS FUERON GUARDADOS EN EL ARCHIVO calculadorafile\n"
msjmultiplicacion: .asciz "MULTIPLICACION\n \nLOS DATOS FUERON GUARDADOS EN EL ARCHIVO calculadorafile\n"
msjdivision: .asciz "DIVISION\n \nLOS DATOS FUERON GUARDADOS EN EL ARCHIVO calculadorafile\n"
msjpotenciacion: .asciz "POTENCIACION\n \nLOS DATOS FUERON GUARDADOS EN EL ARCHIVO calculadorafile\n"
//------------------------Formato para las variables ------------------------------
fmtopcion: .asciz "%s"
fmtvalor: .asciz "%d"
/* --------------Seccion de Codigo -----------------------------------------------*/
.text
.global main
main: 
    PUSH {LR}
_FICHERO:
    _CREARFICHERO:    
    MOV R7, #5	    		//Crear o Abrir un archivo Sycall 5 Arbir
    LDR R0, =nuevoarchivo	//Puntero al Archivo
    MOV R1, #0x42 	    	//Contener las banderas del modo que se abrira el archivo --Crear el archivo si no existe 
    LDR R2, =0666           //Permisos al sistema (lectura si, escritura si, ejecutable no)
    SVC 0                   //Ejecutar las intrucciones
    LDR R4,=descriptor      //Puntero a la variable para almacenar el descriptor en una variable
    STR R0, [R4]            //Copiar el descriptor en la varible
    CMP R0, #-1			    //Si ocurre un error devuelve #-1
    BEQ _ERRORFICHERO       //Salta a la etiqueta de _ERRORFICHERO si fue asi

_Bienvenida:
    LDR R0, =bienvenida         //MENSAJE DE BIENVENIDA 
    BL printf                   //INTERRUPCION PARA IMPRIMIR EN PANTALLA
_MENU:
    LDR R0, =menu               //MENSAJE MOSTRANDO EL MENU
    BL printf                   //INTERRUPCION PARA IMPRIMIR EN PANTALLA
    LDR R0,=fmtopcion           /*CAPTURA DE LA OPCION*/   
    LDR R1,=opcion              //GUARDAR EN LA VARIABLE "opcion"
    BL scanf                    //INTERRUPCION PARA LA CAPTURA INGRESADO EN PANTALLA
    B _Validacion               //INMEDIATAMENTE SALTA A LA ETIQUETA "_Validacion"

_SUMA:
    BL _PREGUNTAR 
    _OPSUMA:
                        /*CARGAR EL VALOR A LOS REGISTRO */
    LDR R2,=valor1          //CARGA LA DIRECCION DE LA VARIABLE valor1 A R2
    LDR R2,[R2]             //CARGA EL VALOR CONNTENIDO EN EL REGISTRO A R2
    LDR R3,=valor2          //CARGA LA DIRECCION DE LA VARIABLE valor2 A R3
    LDR R3,[R3]             //CARGA EL VALOR CONNTENIDO EN EL REGISTRO A R3
                            /* OPERACION DE SUMA */
    ADD R1,R2,R3            //R1=R2+1   -> 10 = 7 + 3
    LDR R4,=total           //CARGA AL REGISTRO EL PUNTERO A LA VARIABLE TOTAL
    STR R1,[R4]             //GUARDA EL VALOR EN LA VARIABLE
    LDR R0,=msjresultado
    BL printf
    LDR R0,=msjsuma
    BL printf
    MOV R7,#4               //SE LLAMA SYSCALL PARA ESCRIBIR 
    LDR R0,=descriptor      //SE LLAMA AL PUNTERO DE LA VARIABLE QUE CONTIENE AL DESCRIPTOR 
    LDR R0,[R0]             //SE CARGA EL VALOR DEL DESCRIPTOR
    LDR R1,=msjsuma         //COMO PRIMER PARAMETRO LE PASAMOS EL MENSAJE DE SUMA
    MOV R2,#6               //MOVEMOS LA CANTIDAD MAXIMA A ESCRIBIR QUE ES #1
    SVC 0                   //EJECUTAMOS LAS INTRUCCIIONES
    BL FIN
_RESTA:
    BL _PREGUNTAR
 _OPRESTA:
    LDR R2,=valor1          //CARGA LA DIRECCION DE LA VARIABLE valor1 A R2
    LDR R2,[R2]             //CARGA EL VALOR CONNTENIDO EN EL REGISTRO A R2
    LDR R3,=valor2          //CARGA LA DIRECCION DE LA VARIABLE valor2 A R3
    LDR R3,[R3]             //CARGA EL VALOR CONNTENIDO EN EL REGISTRO A R3    
                            /* OPERACION DE RESTA */
    SUB R1,R2,R3            //R1=R2+1   -> 4 = 7 - 3 
                            /*MOSTRAR EL RESULTADO */
    LDR R4,=total           //CARGA AL REGISTRO EL PUNTERO A LA VARIABLE TOTAL
    STR R1,[R4]             //GUARDA EL VALOR EN LA VARIABLE
    LDR R0,=msjresultado    //MENSAJE DE RESULTADO
    BL printf               //INSTRUCCION PARA IMPRIMIR EN PANTALLA
    LDR R0,=msjresta        //MENSAJE PARA SABER QUE OPERACION FUE
    BL printf               //INTRUCCION PARA IMPRIMIR EN PANTALLA
    MOV R7,#4               //SE LLAMA SYSCALL PARA ESCRIBIR 
    LDR R0,=descriptor      //SE LLAMA AL PUNTERO DE LA VARIABLE QUE CONTIENE AL DESCRIPTOR 
    LDR R0,[R0]             //SE CARGA EL VALOR DEL DESCRIPTOR
    LDR R1,=msjresta       //COMO PRIMER PARAMETRO LE PASAMOS EL MENSAJE DE RESTA
    MOV R2,#7              //MOVEMOS LA CANTIDAD MAXIMA A ESCRIBIR QUE ES #1
    SVC 0                   //EJECUTAMOS LAS INTRUCCIIONES    
    BL FIN
_MULTIPLICACION:
    BL _PREGUNTAR
    _OPMULTIPLICACION:
                                /*CARGAR EL VALOR A LOS REGISTRO */
    LDR R2,=valor1              //CARGA LA DIRECCION DE LA VARIABLE valor1 A R2
    LDR R2,[R2]                 //CARGA EL VALOR CONNTENIDO EN EL REGISTRO A R2
    LDR R3,=valor2              //CARGA LA DIRECCION DE LA VARIABLE valor2 A R3
    LDR R3,[R3]                 //CARGA EL VALOR CONNTENIDO EN EL REGISTRO A R3
                                /* OPERACION DE MULTIPLICACION */
    MUL R1,R2,R3                //R1=R2+1   -> 12 = 4 * 3 
    LDR R4,=total               //CARGA AL REGISTRO EL PUNTERO A LA VARIABLE TOTAL
    STR R1,[R4]                 //GUARDA EL VALOR EN LA VARIABLE
    LDR R0,=msjresultado        //MENSAJE DE RESULTADO
    BL printf                   //INSTRUCCION PARA IMPRIMIR EN PANTALLA
    LDR R0,=msjmultiplicacion   //MENSAJE PARA SABER QUE OPERACION FUE
    BL printf                   //INTRUCCION PARA IMPRIMIR EN PANTALLA
    MOV R7,#4                   //SE LLAMA SYSCALL PARA ESCRIBIR 
    LDR R0,=descriptor          //SE LLAMA AL PUNTERO DE LA VARIABLE QUE CONTIENE AL DESCRIPTOR 
    LDR R0,[R0]                 //SE CARGA EL VALOR DEL DESCRIPTOR
    LDR R1,=msjmultiplicacion   //COMO PRIMER PARAMETRO LE PASAMOS EL MENSAJE DE MULTIPLICACION
    MOV R2,#15                  //MOVEMOS LA CANTIDAD MAXIMA A ESCRIBIR QUE ES #1
    SVC 0                       //EJECUTAMOS LAS INTRUCCIIONES
    BL FIN
_DIVISION:
    BL _PREGUNTAR
    _OPDIVISION:
         LDR R2,=valor1          //CARGA LA DIRECCION DE LA VARIABLE valor1 A R2
        LDR R2,[R2]             //CARGA EL VALOR CONNTENIDO EN EL REGISTRO A R2
        MOV R0,#-1
        MOV R6,#0
        MOV R7,#0
        CMP R2,#0
        BLGE _POSITIVODIV1
        MUL R2,R2,R0
        MOV R6,#-1
        _POSITIVODIV1:
        LDR R3,=valor2          //CARGA LA DIRECCION DE LA VARIABLE valor2 A R3
        LDR R3,[R3]             //CARGA EL VALOR CONNTENIDO EN EL REGISTRO A R3        
        CMP R3,#0
        BLGE _POSITIVODIV2
        MUL R3,R3,R0
        MOV R7,#-1
        _POSITIVODIV2:
        MOV R1,#0               //SETIAR EL REGISTRO R1 -> #0
        MOV R4,R2               //COPIAR LO QUE CONTIENE EL REGISTRO R2 -> R4
        CMP R3,#0               //COMPARA EL CONTENIDO DE R3 CON EL VALOR DE #0
        BEQ _ErrorDivision      //SI ES ASI SALTA A LA ETIQUETA _ErrorDivision
        _ciclo:                 /*  DIVISION */
        Cmp R2,R3           //Estructura WHILE  IF (R2<R3) -->30<6 -->24<6 ... 0<6
        BLO _continue       //Si es asi ejecuta la instuccion de salto a la etiqueta _continue   
        SUB R2,R2,R3        //R2=R2-R1 --> 24=30-6 ->18=24-6 ...-->R2<--0= 6-6 R2:Residuo
        ADD R1,R1,#1        //R1 Cociente -->R1+=1 -> R1+=1 ... --> R2 = 5
        BAL _ciclo          //Vuelve a la condicion de comparacion
        _continue:
        LDR R5,=total           //CARGA AL REGISTRO EL PUNTERO A LA VARIABLE TOTAL
        CMP R6,R7
        BLEQ _totaldiv
        CMP R6,#-1
        BLNE _CambiarSigno
        CMP R7,#-1
        BLEQ _CambiarSigno
        _CambiarSigno:
            MUL R1,R1,R0
        _totaldiv:
        STR R1,[R5]             //GUARDA EL VALOR EN LA VARIABLE
        LDR R0,=msjresultado    //MENSAJE DE RESULTADO
        BL printf               //INSTRUCCION PARA IMPRIMIR EN PANTALLA
        LDR R0,=msjdivision        //MENSAJE PARA SABER QUE OPERACION FUE
        BL printf               //INTRUCCION PARA IMPRIMIR EN PANTALLA
        MOV R7,#4               //SE LLAMA SYSCALL PARA ESCRIBIR 
        LDR R0,=descriptor      //SE LLAMA AL PUNTERO DE LA VARIABLE QUE CONTIENE AL DESCRIPTOR 
        LDR R0,[R0]             //SE CARGA EL VALOR DEL DESCRIPTOR
        LDR R1,=msjdivision     //COMO PRIMER PARAMETRO LE PASAMOS MENSAJE DE DIVISION
        MOV R2,#10             //MOVEMOS LA CANTIDAD MAXIMA A ESCRIBIR QUE ES #1
        SVC 0                   //EJECUTAMOS LAS INTRUCCIIONES
    BL FIN
_POTENCIA:
    BL _PREGUNTAR
    _OPPOTENCIA: 
                                /*CARGAR EL VALOR A LOS REGISTRO */
        LDR R2,=valor1          //CARGA LA DIRECCION DE LA VARIABLE valor1 A R2
        LDR R2,[R2]             //CARGA EL VALOR CONNTENIDO EN EL REGISTRO A R2
        LDR R3,=valor2          //CARGA LA DIRECCION DE LA VARIABLE valor2 A R3
        LDR R3,[R3]             //CARGA EL VALOR CONNTENIDO EN EL REGISTRO A R3                
        MOV R4,R2                //R4 <- R2    -- R2 = 2
        MOV R5,R3                //R5 <- R3
        _cicloPot:               /*POTENCIACION*/
            CMP R3,#1            //Si el exponente es 1 es valor sigue siendo igual  -->R3=3
            BEQ _continuePOT     //Si es 1 saltara el procedimiento    
            MUL R4,R4,R2         //R4=R4*R2-> 4=2*2 --> 8=4*2 --> 16=8*2
            SUB R3,R3,#1         //R3 controlador --> 2=3-1 -->1=2-1 
            BL _cicloPot         //VOLVERA A LA ETIQUETA _CicloPot para volver a comparar
        _continuePOT:
        LDR R5,=total           //CARGA AL REGISTRO EL PUNTERO A LA VARIABLE TOTAL
        STR R4,[R5]             //GUARDA EL VALOR EN LA VARIABLE
        LDR R0,=msjresultado    //MENSAJE DE RESULTADO
        BL printf               //INSTRUCCION PARA IMPRIMIR EN PANTALLA
        LDR R0,=msjresta        //MENSAJE PARA SABER QUE OPERACION FUE
        BL printf               //INTRUCCION PARA IMPRIMIR EN PANTALLA
        MOV R7,#4               //SE LLAMA SYSCALL PARA ESCRIBIR 
        LDR R0,=descriptor      //SE LLAMA AL PUNTERO DE LA VARIABLE QUE CONTIENE AL DESCRIPTOR 
        LDR R0,[R0]             //SE CARGA EL VALOR DEL DESCRIPTOR
        LDR R1,=msjpotenciacion //COMO PRIMER PARAMETRO LE PASAMOS EL MENSAJE DE POTENCIA
        MOV R2,#14               //MOVEMOS LA CANTIDAD MAXIMA A ESCRIBIR QUE ES #1
        SVC 0                   //EJECUTAMOS LAS INTRUCCIIONES

    BL FIN
FIN:
_CARGARDATOS:
                        /*Variable para cargar los datos */
    LDR R2,=dato        //Cargar el puntero de la variable dato
    LDR R2,[R2]         //Carga el valor al mismo registro
    ADD R2,#1           //Le suma #1 al valor
    LDR R3,=dato        //Carga de nuevo el puntero 
    STR R2,[R3]         //Almacena el valor el valor en la variable
    CMP R2,#1           //Compara si el valor del dato y salta a la etiqueta indicada
    BLEQ _CargarValor1
    CMP R2,#2
    BLEQ _CargarValor2
    CMP R2,#3
    BLEQ _CargarTotal
    BL ESC              //Cuando termine saltara al final del programa
    _CargarValor1:      //Etiqueta para guardar el primer valor
        LDR R1,=valor1  //Carga el puntero del valor1
        LDR R1,[R1]     //carga el valor al mismo registro
        CMP R1,#0
        BLGE _POSITIVOV1
        MOV R3,#-1         //SETIAMOS EL VALOR CON EL VALOR #-1
        MUL R1,R1,R3       //MULTIPLICAMOS EL VALOR CON #-1 PARA HACERLO POSITIVO
        LDR R2,=numero     //carga el puntero de la variable a utilizar en el proceso de guardar
        STR R1,[R2]        //Carga el valor a la variable 
        
        MOV R7,#4          //SE LLAMA SYSCALL PARA ESCRIBIR 
        LDR R0,=descriptor //SE LLAMA AL PUNTERO DE LA VARIABLE QUE CONTIENE AL DESCRIPTOR 
        LDR R0,[R0]        //SE CARGA EL VALOR DEL DESCRIPTOR
        LDR R1,=signo      //COMO PRIMER PARAMETRO LE PASAMOS EL VALOR DEL DIGITO
        MOV R2,#1          //MOVEMOS LA CANTIDAD MAXIMA A ESCRIBIR QUE ES #1
        SVC 0              //EJECUTAMOS LAS INTRUCCIIONES
        BL _CIFRAS         //salta al proceso de guardar
        _POSITIVOV1:
            LDR R2,=numero //carga el puntero de la variable a utilizar en el proceso de guardar
            STR R1,[R2]    //Carga el valor a la variable 
            BL _CIFRAS     //salta al proceso de guardar
    _CargarValor2:         //Etiqueta para guardar el segundo valor
        LDR R1,=valor2     //Carga el puntero del valor2
        LDR R1,[R1]        //carga el valor al mismo registro
        CMP R1,#0
        BLGE _POSITIVOV2
        MOV R3,#-1         //SETIAMOS EL VALOR CON EL VALOR #-1
        MUL R1,R1,R3       //MULTIPLICAMOS EL VALOR CON #-1 PARA HACERLO POSITIVO
        LDR R2,=numero     //carga el puntero de la variable a utilizar en el proceso de guardar
        STR R1,[R2]        //Carga el valor a la variable 
        MOV R7,#4          //SE LLAMA SYSCALL PARA ESCRIBIR 
        LDR R0,=descriptor //SE LLAMA AL PUNTERO DE LA VARIABLE QUE CONTIENE AL DESCRIPTOR 
        LDR R0,[R0]        //SE CARGA EL VALOR DEL DESCRIPTOR
        LDR R1,=signo      //COMO PRIMER PARAMETRO LE PASAMOS EL VALOR DEL DIGITO
        MOV R2,#1          //MOVEMOS LA CANTIDAD MAXIMA A ESCRIBIR QUE ES #1
        SVC 0              //EJECUTAMOS LAS INTRUCCIIONES
        BL _CIFRAS         //salta al proceso de guardar
        _POSITIVOV2:        
        LDR R2,=numero     //carga el puntero de la variable a utilizar en el proceso de guardar
        STR R1,[R2]        //Carga el valor a la variable
        BL _CIFRAS         //salta al proceso de guardar
    _CargarTotal:
        LDR R1,=total      //Carga el puntero de la variable total
        LDR R1,[R1]
        CMP R1,#0
        BLGE _POSITIVOTT
        MOV R3,#-1         //SETIAMOS EL VALOR CON EL VALOR #-1
        MUL R1,R1,R3       //MULTIPLICAMOS EL VALOR CON #-1 PARA HACERLO POSITIVO
        LDR R2,=numero     //carga el puntero de la variable a utilizar en el proceso de guardar
        STR R1,[R2]        //Carga el valor a la variable 
        MOV R7,#4          //SE LLAMA SYSCALL PARA ESCRIBIR 
        LDR R0,=descriptor //SE LLAMA AL PUNTERO DE LA VARIABLE QUE CONTIENE AL DESCRIPTOR 
        LDR R0,[R0]        //SE CARGA EL VALOR DEL DESCRIPTOR
        LDR R1,=signo      //COMO PRIMER PARAMETRO LE PASAMOS EL VALOR DEL DIGITO
        MOV R2,#1          //MOVEMOS LA CANTIDAD MAXIMA A ESCRIBIR QUE ES #1
        SVC 0              //EJECUTAMOS LAS INTRUCCIIONES
        BL _CIFRAS         //salta al proceso de guardar
        _POSITIVOTT:
        LDR R2,=numero     //carga el puntero de la variable a utilizar en el proceso de guardar
        STR R1,[R2]
        BL _CIFRAS         //salta al proceso de guardar
ESC:

    BL _CERRARFICHERO
FINISH:
    POP {PC}

_Exit:
        mov R7, #1         //Llama SYSCALL exit = 1
        SWI 0              //Ejecuta llamada a la funcion del sistema
_Validacion:   
        LDR R3,=opcion     //CARGA EN REGISTRO R3 LA DIRECCION DE LA VARIABLE opcion
        LDR R3,[R3]        //CARGA EN REGISTRO R3 LO QUE CONTIENE EL REGISTRO R3
        CMP R3,#53         //COMPARA EL CONTENIDO CON #53 --> EN CODIGO ASCII #53 --> 5
        BHI _ErrorMenu     //SI ES MAYOR SALTA A LA ETIQUETA _ErrorMenu
        CMP R3,#49         //COMPARA EL CONTENIDO CON #49 --> EN CODIGO ASCII #49 --> 1
        BLT _ErrorMenu     //SI ES MENOR SALTA A LA ETIQUETA _ErrorMenu
        SUB R1, R3,#48     //POSTERIOR SE COMPARA Y SALTARA A LA ETIQUETA CORRESPONDIENTE
        B _Opcion          //SALTO A LA ETIQUETA DE _Opcion
_Opcion:
    CMP R1,#1              //COMPARA SI LA OPCION ES #1 DEL REGISTRO R1 -> opcion
    BEQ _SUMA              //SI ES ASI SALTA A LA ETIQUETA _SUMA
    CMP R1,#2              //COMPARA SI LA OPCION ES #2 DEL REGISTRO R1 -> opcion
    BEQ _RESTA             //SI ES ASI SALTA A LA ETIQUETA _RESTA
    CMP R1,#3              //COMPARA SI LA OPCION ES #3 DEL REGISTRO R1 -> opcion
    BEQ _MULTIPLICACION    //SI ES ASI SALTA A LA ETIQUETA _MULTIPLICACION
    CMP R1,#4              //COMPARA SI LA OPCION ES #4 DEL REGISTRO R1 -> opcion
    BEQ _DIVISION          //SI ES ASI SALTA A LA ETIQUETA _DIVISION
    CMP R1,#5              //COMPARA SI LA OPCION ES #5 DEL REGISTRO R1 -> opcion
    BEQ _POTENCIA          //SI ES ASI SALTA A LA ETIQUETA _POTENCIACION
_PREGUNTAR:
    _PRIMERVALOR:
        LDR R0,=msjvalor1  //CARGA EL MENSAJE QUE ESTA EN LA VARIABLE msjvalor1 a R0 
        Bl printf          //HACE LA INTERRUPCION PARA IMPRIMIR EN PANTALLA 
                           /*CAPTURA DEL VALOR 1 */
        LDR R0,=fmtvalor   //CARGA EL FORMATO DEL DATO INGRESADO
        LDR R1,=valor1     //ALMACENA EL DATO EN LA VARIABLE VALOR 1
        Bl scanf           //HACE LA INTERRUPCION DE ESCANEAR LO INGRESADO EN PANTALLA
    _SEGUNDOVALOR:
        LDR R0,=msjvalor2  //CARGA EL MENSAJE QUE ESTA EN LA VARIABLE msjvalor2 a R0 
        Bl printf          //HACE LA INTERRUPCION PARA IMPRIMIR EN PANTALLA 
                           /*CAPTURA DEL VALOR 2 */
        LDR R0,=fmtvalor   //CARGA EL FORMATO DEL DATO INGRESADO
        LDR R1,=valor2     //ALMACENA EL DATO EN LA VARIABLE VALOR 2
        Bl scanf           //HACE LA INTERRUPCION DE ESCANEAR LO INGRESADO EN PANTALLA
    
        LDR R1,=opcion     //CARGA EN REGISTRO R1 LA DIRECCION DE LA VARIABLE opcion
        LDR R1,[R1]        //CARGA EN REGISTRO R1 LO QUE CONTIENE EL REGISTRO R1
        SUB R1,R1,#48      //RECORDEMOS QUE EL REGISTRO CONTIENE UN DATO STRING EN CODIGO ASCII
        CMP R1,#1          //COMPARA SI LA OPCION ES #1 DEL REGISTRO R1 -> opcion
        BEQ _OPSUMA
        CMP R1,#2          //COMPARA SI LA OPCION ES #2 DEL REGISTRO R1 -> opcion
        BEQ _OPRESTA
        CMP R1,#3          //COMPARA SI LA OPCION ES #3 DEL REGISTRO R1 -> opcion
        BEQ _OPMULTIPLICACION
        CMP R1,#4          //COMPARA SI LA OPCION ES #4 DEL REGISTRO R1 -> opcion
        BEQ _OPDIVISION
        CMP R1,#5          //COMPARA SI LA OPCION ES #5 DEL REGISTRO R1 -> opcion
        BEQ _OPPOTENCIA

_ALTERNATIVA:
   _CIFRAS:
        LDR R2,=numero     //CARGA EL PUNTERO A LA VARIABLE QUE NECESITAMOS SABER CUANTAS CIFRAS TIENE     --> 245
        LDR R2,[R2]        //CARGA EL VALOR AL MISMO REGISTRO
        MOV R3,#0          //SETEAR EL REGISTRO R3 QUE SERVIRA COMO CONTADOR PARA LAS CIFRAS -->0
    _WHILE:
        CMP R2, #0         //COMPARA EL VALOR SI TIENE UN DIGITO -->IF(245<0)-->IF(24<0)-->IF(4<0)
        BLE _ENDWHILE      //DE SER 0 O MENOR SALTA INMEDIATAMENTE A LA SEGUNDA PARTE -->SI LO ES
        ADD R3,R3,#1       //SUMA #+1 AL CONTADOR DE CIFRAS YA QUE NO ES CERO --> R3+=1  --> R3+=1 --> R3+=1  
        MOV R1,#0          //SETEA EL REGISTRO R1 A CERO   R1=0
        _ciclowhile:       /*DIVIENDO LA CIFRA */
            Cmp R2,#10     
//COMPARA A 10 PARA SABER SI ES DE UN DIGITO O MAS -->IF(245<10)-->IF(235<10)...-->IF(24<10) 
            BLO _continuewhile// SALTA INMEDIATAMENTE A LA ETIQUETA _continuewhile
            SUB R2,R2,#10     //residuo --> 235 =245-10-->225=235-10... R2 = 5 =15-10 ... R2=4 =14-10
            ADD R1,R1,#1      //cociente -->R1+=1 --> Re1+=1... R1=24
            BAL _ciclowhile   // ES MAYOR A #10 --> IF (235<10)-->IF(225<10)-->IF(5<10)
        _continuewhile:
            MOV R2,R1     //EL VALOR DE COCIENTE SE COPIAR A R2 COMO UN NUEVO DIVIENDO --> R2-->R1 = 24 ...--> R2-->R1 = 4
            BL _WHILE     //SALTA A ETIQUETA _WHILE
    _ENDWHILE:
        LDR R1,=cifras    //CARGA EL PUNTERO A LA VARIABLE CIFRA --->245
        STR R3,[R1]       //COPIA EL VALOR DE R3 QUE ES CONTADOR PARA LA CIFRAS A LA VARIABLE CIFRAS
                          /*PARTE 2 */
        MOV R6,R3         //COPIAMOS LO EL CONTADOR DE CIFRAS EN R6 PORQUE LO OCUPAREMOS COMO INDICES
    _SEPARAR:
        LDR R2,=numero    //CARGAMOS EL PUNTERO DE LA VARIABLE NUMERO -- Nuevo valor de numero porque el residuo se cargo en numero
        LDR R2,[R2]       //CARGAMOS EL VALOR AL REGISTRO
        MOV R5,R2         //COPIAMOS EL REGISTRO R2 QUE CONTIENE EL NUMERO A R5
        MOV R4,#1         //SETIAMOS EL VALOR DE R4 QUE SERVIRA COMO EXPONENTE       
        CMP R6,#0         //COMPARAMOS SI EL VALOR DE CIFRAS ES CERO
        BLEQ _SALTO       //SI ES SALI SALE DE INMEDIATO 
    _EXP:
        MOV R1,#10        //SETIAMOS EL VALOR DE R1 A 10 QUE SERVIRA COMO MULTIPLO
        CMP R3,#1         //COMPARAMOS EL VALOR DE CIFRAS SI ES 1 -->CIFRAS 3
        BLEQ _separacion  //SI ES ASI SALTA INMEDIADO A LA ETIQUETA _separacion
        MUL R4,R4,R1      //R4=R4*R1 --> 10=1*10 -->100=10*10
        SUB R3,R3,#1      //CIFRAS -1 =2 -->CIFRAS -1 =1
        BL _EXP
    _separacion:
        MOV R1,#0         //SETIAMOS DE NUEVO R1 QUE SERVIRA COMO DIGITO
        _ciclosep: 
        Cmp R5,R4        //IF (NUMERO<EXPOENTE)
        BLO _continuesep //SI ES SALTA 
        SUB R5,R5,R4        //residuo        245/100 -> 2 ; Residuo 45 --> 45/10 -> 4, Residuo 5 
        ADD R1,R1,#1        //cociente
        BAL _ciclosep
        _continuesep:
        LDR R2,=numero    //CARGA EL PUNTERO A LA VARIABLE NUMERO
        STR R5,[R2]       //Carga el residuo de la division como nuevo valor numero --> numero=45
        LDR R3,=digito    //Valor digito es 2
        ADD R1,R1,#48     //En codigo ascii en #48 -> 0      Ascii #50-> 2  
        STR R1,[R3]       //CARGAMOS EL VALOR PARA IMPRIMIRLO CON CODIGO ASCII
    _CARGARFICHERO:         /*GUARDAR EN EL ARCHIVO */
        MOV R7,#4           //SE LLAMA SYSCALL PARA ESCRIBIR 
        LDR R0,=descriptor  //SE LLAMA AL PUNTERO DE LA VARIABLE QUE CONTIENE AL DESCRIPTOR 
        LDR R0,[R0]         //SE CARGA EL VALOR DEL DESCRIPTOR
        LDR R1,=digito      //COMO PRIMER PARAMETRO LE PASAMOS EL VALOR DEL DIGITO
        MOV R2,#1           //MOVEMOS LA CANTIDAD MAXIMA A ESCRIBIR QUE ES #1
        SVC 0               //EJECUTAMOS LAS INTRUCCIIONES
        SUB R6,R6,#1        //CIFRAS = CIFRAS - 1 -> cifras eran 3,ahora seran 2 que fal    tan pasar
        MOV R3,R6           //Cifras = 3, Luego cifras = 2 SE COPIAR PARA TRABAJER LOS INDICES NUEVOS
        BL _SEPARAR
    _SALTO:
        MOV R7,#4           //SE LLAMA SYSCALL PARA ESCRIBIR 
        LDR R0,=descriptor  //SE LLAMA AL PUNTERO DE LA VARIABLE QUE CONTIENE AL DESCRIPTOR 
        LDR R0,[R0]         //SE CARGA EL VALOR DEL DESCRIPTOR
        LDR R1,=salto       //COMO PRIMER PARAMETRO LE PASAMOS EL VALOR DEL DIGITO
        MOV R2,#12          //MOVEMOS LA CANTIDAD MAXIMA A ESCRIBIR QUE ES #1
        SVC 0               //EJECUTAMOS LAS INTRUCCIIONES
        BL _CARGARDATOS

_CERRARFICHERO: 
        MOV R7, #6              //CERRAR EL ARCHIvO
        SVC 0                   //EJECUTAR LA INSTRUCCION
        BL FINISH
_ErrorMenu:
        LDR R0, =error_menu     //MENSAJE DE ERROR EN EL MENU
        BL printf               //INTERRUPCION PARA IMPRIMIR EN PANTALLA
        BL _MENU                //SALTA A _MENU VOLVIENTO A PREGUNTAR LA OPCION DEL MENU
_ErrorDivision:
        LDR R0,=errordiv        //MENSAJE DE ERRO QUE NO SE PUEDE DIVIDIR ENTRE 0
        BL printf               //INTERRUPCION PARA IMPRIMIR 
        B _SEGUNDOVALOR         //VOLVER A PREGUNTAR EL SEGUNDO VALOR
_ERRORFICHERO:
        LDR R0,=errormsj        //MENSAJE DE ERROR
        BL printf               //INSTRUCCION DE MOSTRAR EN PANTALLA
        BL FINISH               //SALTAR A LA ETIQUETA FINISH        
