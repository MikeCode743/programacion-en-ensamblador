/*Archivo*/

        .data
errormsj:       .asciz "Error al crear el archivo\n"
cadenamsj:      .asciz "Ingrese un digito \n"
nuevoarchivo:   .asciz "file"
frase: .asciz "total es: %d\n"

frasecifras: .asciz "\n digito %d"


//--------------Prueba Variables----------//
numero: .word 0
digito: .word 0
cifras: .word 0

descriptor: .space 4
permiso: .space 4

fmt: .asciz "%d"



        .text
        .global main

main:
    PUSH    {LR}

_FICHERO:
    _CREARFICHERO:    
        MOV R7, #5	    		//Crear o Abrir un archivo Sycall 5 Abrir
        LDR R0, =nuevoarchivo	//Puntero al Archivo
        MOV R1, #0x42 	    	//Contener las banderas del modo que se abrira el archivo --Crear el archivo si no existe 
        LDR R2, =0666           //Permisos al sistema (lectura si, escritura si, ejecutable no) 6=110
        SVC 0                   //Ejecutar las intrucciones
        LDR R4,=descriptor      //Puntero a la variable para almacenar el descriptor en una variable
        STR R0, [R4]            //Copiar el descriptor en la varible
        CMP R0, #-1			    //Si ocurre un error devuelve #-1
        BEQ _ERRORFICHERO       //Salta a la etiqueta de _ERRORFICHERO si fue asi
    //Solicitar cadena
        LDR R0, =cadenamsj
        BL printf
        LDR R0,=fmt
        LDR R1,=numero
        BL scanf
                                
_ALTERNATIVA:
   _CIFRAS:
        LDR R2,=numero          //CARGA EL PUNTERO A LA VARIABLE QUE NECESITAMOS SABER CUANTAS CIFRAS TIENE     -->245
        LDR R2,[R2]             //CARGA EL VALOR AL MISMO REGISTRO
        MOV R3,#0               //SETEAR EL REGISTRO R3 QUE SERVIRA COMO CONTADOR PARA LAS CIFRAS-->0
    _WHILE:                     
        CMP R2, #0              //COMPARA EL VALOR SI TIENE UN DIGITO -->IF(245<0)-->IF(24<0)-->IF(4<0)
        BLE _ENDWHILE           //DE SER 0 O MENOR SALTA INMEDIATAMENTE A LA SEGUNDA PARTE -->SI LO ES
        ADD R3,R3,#1            //SUMA #+1 AL CONTADOR DE CIFRAS YA QUE NO ES CERO --> R3+=1  --> R3+=1 --> R3+=1 R3=3  
        MOV R1,#0               //SETEA EL REGISTRO R1 A CERO   R1=0
        _ciclowhile:            /*DIVIENDO LA CIFRA */ 
            Cmp R2,#10          //COMPARA A 10 PARA SABER SI ES DE UN DIGITO O MAS -->IF(245<10)-->IF(235<10)...-->IF(24<10) 
            BLO _continuewhile  //SI SOLO TIENE UNA CIFRA SALTA INMEDIATAMENTE A LA ETIQUETA _continuewhile
            SUB R2,R2,#10       //residuo -- DE SER MAYOR A UNA CIFRA HACE ESTA RESTA--> 235 =245-10-->225=235-10... R2 = 5 =15-10 ... R2=4 =14-10
            ADD R1,R1,#1        //cociente -- SE CALCULA CUANTAS VECES SE RESTO EL VALOR -->R1+=1 --> R1+=1... R1=24
            BAL _ciclowhile     //VUELVE AL CICLO SI UN EL VALOR ES MAYOR A #10 --> IF (235<10)-->IF(225<10)-->IF(5<10)
        _continuewhile:
            MOV R2,R1           //EL VALOR DE COCIENTE SE COPIAR A R2 COMO UN NUEVO DIVIENDO --> R2-->R1 = 24 ...--> R2-->R1 = 4
            BL _WHILE           //SALTA A ETIQUETA _WHILE
    _ENDWHILE:
        LDR R1,=cifras          //CARGA EL PUNTERO A LA VARIABLE CIFRA --->245
        STR R3,[R1]             //COPIA EL VALOR DE R3 QUE ES CONTADOR PARA LA CIFRAS A LA VARIABLE CIFRAS
                                /*PARTE 2 */
        MOV R6,R3               //COPIAMOS LO EL CONTADOR DE CIFRAS EN R6 PORQUE LO OCUPAREMOS COMO INDICES
    _SEPARAR:
        LDR R2,=numero          //CARGAMOS EL PUNTERO DE LA VARIABLE NUMERO -- Nuevo valor de numero porque el residuo se cargo en numero
        LDR R2,[R2]             //CARGAMOS EL VALOR AL REGISTRO
        MOV R5,R2               //COPIAMOS EL REGISTRO R2 QUE CONTIENE EL NUMERO A R5
        MOV R4,#1               //SETIAMOS EL VALOR DE R4 QUE SERVIRA COMO EXPONENTE            
        CMP R6,#0               //COMPARAMOS SI EL VALOR DE CIFRAS ES CERO
        BLEQ _CERRARFICHERO      //SI ES SALI SALE DE INMEDIATO 245 100
    _EXP:
        MOV R1,#10              //SETIAMOS EL VALOR DE R1 A 10 QUE SERVIRA COMO MULTIPLO
        CMP R3,#1               //COMPARAMOS EL VALOR DE CIFRAS SI ES 1 -->CIFRAS 3
        BLEQ _separacion        //SI ES ASI SALTA INMEDIADO A LA ETIQUETA _separacion
        MUL R4,R4,R1            //R4=R4*R1 --> 10=1*10 -->100=10*10   -> 10
        SUB R3,R3,#1            //CIFRAS -1 =2 -->CIFRAS -1 =1
        BL _EXP
    _separacion:
        MOV R1,#0               //SETIAMOS DE NUEVO R1 QUE SERVIRA COMO DIGITO
        _ciclosep: 
            Cmp R5,R4           //IF (NUMERO<EXPOENTE) 245/100--->2 ;45--> 4 5   
            BLO _continuesep    //SI ES SALTA 
            SUB R5,R5,R4        //residuo        245/100 -> 2 ; Residuo 45 --> 45/10 -> 4, Residuo 5 
            ADD R1,R1,#1        //cociente
            BAL _ciclosep
        _continuesep:
        LDR R2,=numero           //CARGA EL PUNTERO A LA VARIABLE NUMERO
        STR R5,[R2]             //Carga el residuo de la division como nuevo valor numero --> numero=45
        LDR R3,=digito          //Valor digito es 2
        ADD R1,R1,#48           //En codigo ascii en #48 -> 0      Ascii #50-> 2  
        STR R1,[R3]             //CARGAMOS EL VALOR PARA IMPRIMIRLO CON CODIGO ASCII

    _CARGARFICHERO:             /*GUARDAR EN EL ARCHIVO */
        MOV R7,#4               //SE LLAMA SYSCALL PARA ESCRIBIR 
        LDR R0,=descriptor      //SE LLAMA AL PUNTERO DE LA VARIABLE QUE CONTIENE AL DESCRIPTOR 
        LDR R0,[R0]             //SE CARGA EL VALOR DEL DESCRIPTOR
        LDR R1,=digito          //COMO PRIMER PARAMETRO LE PASAMOS EL VALOR DEL DIGITO-->50= 2 4 5
        MOV R2,#1               //MOVEMOS LA CANTIDAD MAXIMA A ESCRIBIR QUE ES #1
        SVC 0                   //EJECUTAMOS LAS INTRUCCIIONES
        SUB R6,R6,#1            //CIFRAS = CIFRAS - 1 -> cifras eran 3,ahora seran 2 que fal    tan pasar
        MOV R3,R6               //Cifras = 3, Luego cifras = 2 SE COPIAR PARA TRABAJER LOS INDICES NUEVOS
    BL _SEPARAR

    _CERRARFICHERO: 
     
        MOV R7, #6              //CERRAR EL ARCHIvO
        SVC 0                   //EJECUTAR LA INSTRUCCION

    POP {pc}

_exit:
    MOV R7,#1
    SWI 0

_ERROR:
    LDR R0,=errormsj
    BL printf

_ERRORFICHERO:
    LDR R0,=errormsj            //MENSAJE DE ERROR
    BL printf                   //INSTRUCCION DE MOSTRAR EN PANTALLA
    BL _exit                    //SALTAR A LA ETIQUETA FINISH
        